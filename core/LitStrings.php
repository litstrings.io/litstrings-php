<?php


namespace LitStrings;


use DOMDocument;

class LitStrings
{

    public static $version = '0.0.7';
    public static $locations;
    public static $auto_meta = true;
    public static $autoescape = false;
    public static $context_cache = array();

    public $strings = array();

    protected $lng_code = null;
    protected $path = null;
    protected $project_root = null;

    private $strict, $lngCookieName, $originalLng;


    /**
     * LitStrings constructor.
     * @param string $project_root
     * @param string $originalLng
     * @param string $path
     * @param bool $strict
     */
    public function __construct($project_root, $originalLng = "en-US", $path = "static", $strict = true) {
        //$this->lngCookieName = "ls_lng_tag_$_SERVER[HTTP_HOST]"; //every site has own name?
        $this->lngCookieName = "ls_lng_tag_";
        $this->originalLng = $originalLng;
        $this->project_root = $this->checkPath($project_root);
        $this->path = $this->checkPath($path);
        $this->strict = $strict;
        self::lngController(); //todo this->lng_code
        $this->strings = $this->getStrings();
        self::init();
    }

    /**
     * @codeCoverageIgnore
     */
    protected static function init() {
        if ( !defined('LITSTRINGS_LOADED') ) {
            define('LITSTRINGS_LOADED', true);
        }
    }



    /**
     * @return array|mixed
     */
    protected function getStrings() {
        $rsp = [];
        $arr = [];
        if ($this->lng_code != null) {
            $lngFile = $this->project_root.$this->path.'strings/'.$this->lng_code.'/strings.xml';
            $this->initLngFile($lngFile);

            if (file_exists($lngFile)) {
                $arr = simplexml_load_file($lngFile);
            }


            if (empty($arr)) {
                $baseLngArr = explode("-", $this->lng_code);
                empty($baseLngArr) ? $baseLngArr = explode("_", $this->lng_code) : null;
                if (empty($baseLngArr[0]) || $baseLngArr[0] == "-" || $baseLngArr[0] == "_") { unset($baseLngArr[0]); }
                $locales = $this->getLocalLanguages();
                if (!empty($baseLngArr)) {
                    foreach ($locales as $locale) {
                        if (strpos($locale, strtolower($baseLngArr[0])) !== false) {
                            $lngFile = $this->project_root.$this->path.'strings/'.$locale.'/strings.xml';
                            $this->initLngFile($lngFile);
                            if (file_exists($lngFile)) {
                                $arr = simplexml_load_file($lngFile);
                            }
                        }
                    }
                }
                if (empty($arr)){
                    $lngFile = $this->project_root.$this->path.'strings/'.$this->originalLng.'/strings.xml';
                    $this->initLngFile($lngFile);
                    if (file_exists($lngFile)) {
                        $arr = simplexml_load_file($lngFile);
                    }
                }
                if (empty($arr)){
                    echo "Error loading original strings ¿";
                }

            }

            if ($arr === false) {
                $rsp[] = ["Failed loading language: "];
                foreach(libxml_get_errors() as $error) {
                    $rsp[] = [$error->message];
                }
            } else {
                $json = json_encode($arr);
                $array = json_decode($json, true);
                $rsp = $array;
            }




        }

        return $rsp;
    }


    /**
     * @return array|bool|false
     */
    public function getLocalLanguages() {
        $directories = glob($this->project_root.$this->path.'strings/' . '/*' , GLOB_ONLYDIR);
        foreach ($directories as &$directory) {
            @$directory =  end( explode( "/", $directory ) );
        }
        return !empty($directories) ? $directories : false;
    }

    /**
     * @param $lngFileAbs
     */
    protected function initLngFile($lngFileAbs) {
//        if (!file_exists($lngFileAbs)) {
//            $this->createPath($this->project_root.$this->path.'strings/'.$this->lng_code);
//            //create empty translation file
//            $doc = new DOMDocument('1.0');
//            $doc->formatOutput = true;
//            $root = $doc->createElement('resources');
//            $root = $doc->appendChild($root);
//            foreach([] as $key=>$value) {
//                $em = $doc->createElement($key);
//                $text = $doc->createTextNode($value);
//                $em->appendChild($text);
//                $root->appendChild($em);
//            }
//            $doc->save($lngFileAbs);
//        }
    }

    protected function lngController() {
        isset($_GET['lng']) && !empty($_GET['lng']) ? $lng = strip_tags($_GET['lng']) : $lng = null;
        !is_null($lng) ? $this->setLngCookie($lng) : null;
        $lngCookie = $this->getLngCookie();
        !is_null($lngCookie) ? $this->lng_code = $lngCookie : $this->lng_code = $this->originalLng;
    }

    public function checkPath($path) {
        if (substr($path, -1) !== "/") {
            $path = $path."/"; // todo :D
        }
        return $path;
    }

    /**
     * recursively create a long directory path
     */
    public function createPath($path) {
        if (is_dir($path)) return true;
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
        $return = self::createPath($prev_path);
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }

    /**
     * @return mixed|null
     */
    protected function getLngCookie() {
        $cookie_name = $this->lngCookieName;
        if(!isset($_COOKIE[$cookie_name])) {
            return null;
        } else {
            return $_COOKIE[$cookie_name];
        }
    }

    protected function setLngCookie($data) {
        $cookie_name = $this->lngCookieName;
        $cookie_value = $data;
        setcookie($cookie_name, $cookie_value, time() + (86400 * 365), '/');

        if ($this->strict) { //todo
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            $url = $this->removeParameterFromUrl($actual_link, "lng");
            echo "<script>window.open('".$url."','_self');</script>";
        }

    }

    protected function rmLngCookie() {
        $cookie_name = $this->lngCookieName;
        unset($_COOKIE[$cookie_name]);
        // empty value and expiration one hour before
        $res = setcookie($cookie_name, '', time() - 3600);
    }

    /**
     * @param $url
     * @param bool $permanent
     */
    protected function redirect($url, $permanent = false) {
        header('Location: ' . $url, true, $permanent ? 301 : 302);
        exit();
    }

    /**
     * @param $url
     * @param $key
     * @return mixed|string
     */
    function removeParameterFromUrl($url, $key) {
        $parsed = parse_url($url);
        $path = $parsed['path'];
        unset($_GET[$key]);
        if(!empty(http_build_query($_GET))){
            return $path .'?'. http_build_query($_GET);
        } else return $path;
    }


}