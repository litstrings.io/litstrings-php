LitStrings PHP
====

A [LitStrings PHP][litstrings] is released under the open
source [MIT-license][MIT-license].

LitStrings PHP library

Installation
------------

`$ composer require litstrings/litstrings-php dev-master`

Usage
-----
Just instantiate it in your `index` as shown in example
```
/** Define project's root path as index file's directory */
if ( ! defined( 'THEPATH' ) ) {
    define( 'THEPATH', dirname( __FILE__ ) . '/' );
}

$strings = new \LitStrings\LitStrings(THEPATH);
$context['strings'] = $strings->strings;
```

"Controller"
----------
Lib comes with built in cookie based "language controller", meaning that all you need to do to generate appropriate context translation is to append `?lng=en-US` to anywhere where your `index` reaches, and LitStrings will generate {{ strings }} object for `en-US`. All rest actions/links will be treated with same locale, so you dont need to append `lng` param anymore, unless you want to change locale

Reporting issues
----------------

See our [Contributing to LitStrings Core][contributing] guide.

Support
-------

Have a question? Want to chat? Run into a problem? See our [community][support]
page.

---


[litstrings]: https://litstrings.info
[MIT-license]: http://opensource.org/licenses/mit-license.php
[docs]: https://docs.litstrings.io/installation
[support]: https://litstrings.io/community
[contributing]: https://gitlab.com/litstrings.io/litstrings-php/CONTRIBUTING.md